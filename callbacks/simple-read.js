/**
 * Simple read. Read the content of a file
 * asynchronously and log to the console.
 */
const fs = require('fs');
fs.readFile('./notes.txt', 'utf-8', function(err, content) {
  if (err) {
    return console.log(err);
  }
  console.log(content);
});

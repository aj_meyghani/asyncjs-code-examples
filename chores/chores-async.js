/*
 Example to point out how three tasks can be scheduled
 asynchronously. Also involes one task being dependent
 on the result of the previous task. Example shows
 that even with promises you can end up with a result
 that looks like a synchronous execution.
 */

const {laundry, groceries, cook} = require('./tasks');

/* my chore list (async)

1) do laundry
2) do groceries
3) cook food

to cook food I need to do groceries first (task 3 is dependent on task 2)

Goal: save time, finish everyting in 1 hour

We know laundry takes 1 hour (30 min wash + 30 min dry)
We know groceries takes about 30 min
We know cooking takes about 30 min


0--------------30min--------------1hr--------------1.5h--------------2h
|
start
laundry
     |
   start
  groceries
                   |
                finish
               groceries
                    |
                  put clothes
                    in dryer
                            |             |
                          start          finish
                          cooking       cooking
                                              |
                                            finish laundry
                                                        |
                                                      done! (around 2h)
*/
function doTasksConcurrent() {
  console.time('total concurrent time');
  const laundryTask = laundry();
  const cooking = groceries()
    .then((ingredients) => {
      return cook(ingredients);
    })
    .then((cookResult) => {
      return cookResult;
    });
  return Promise.all([laundryTask, cooking]);
}

doTasksConcurrent()
  .then(function(d) {
    console.log('All result', d);
    console.timeEnd('total concurrent time');
  });

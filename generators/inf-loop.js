function* myGenerator() {
  let i = 0;
  while(true) {
    i += 1;
    yield i;
  }
}

const g = myGenerator();
const v1 = g.next(); // --> { value: 1, done: false }
const v2 = g.next(); // --> { value: 2, done: false }
const v3 = g.next(); // --> { value: 3, done: false }

console.log(v1, v2, v3);

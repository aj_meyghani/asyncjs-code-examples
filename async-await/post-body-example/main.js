const fs = require('fs');
const axios = require('axios');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const readdir = util.promisify(fs.readdir);

function onlyTextFiles(f) {
  return f.split('.')[1] === 'txt';
}

async function main() {
  try {
    const response = await axios.get('https://jsonplaceholder.typicode.com/posts/1');
    const postBody = response.data.body;
    const localFolderList = await readdir('.');
    const textFiles = localFolderList.filter(onlyTextFiles);
    const localFileContent = await readFile(textFiles[0], 'utf-8');
    const finalResult = localFileContent + postBody;
    const writeResult = await writeFile('./final.txt', finalResult);
    return writeResult;
  } catch (error) {
    return Promise.reject(error);
  }
}

main().then(d => console.log(d)).catch(e => console.log(e));


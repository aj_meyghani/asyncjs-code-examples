/*
 * Wrapping the async readFile function in a promise
 */
const fs = require('fs');
function readFile(file, format = 'utf-8') {
  return new Promise((resolve, reject) => {
    fs.readFile(file, format, (err, content) => {
      if(err) return reject(err);
      resolve(content);
    });
  });
}


readFile('./example.txt')
  .then(content => console.log(content))
  .catch(err => console.log(err));

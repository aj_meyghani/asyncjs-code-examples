function* myGenerator() {
  let i = 0;
  while(i < 2) {
    i += 1;
    yield i;
  }
}

const g = myGenerator();
let next = g.next().value;
while(next) {
  console.log(next);
  next = g.next().value;
}

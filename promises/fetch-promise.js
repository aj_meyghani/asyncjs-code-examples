/* Using the fetch api in the browser */
var url = 'https://jsonplaceholder.typicode.com/posts/1';
fetch(url)
.then(function(response) {
  return response.json();
}).then(function(post) {
  console.log(post.id, post.title);
});

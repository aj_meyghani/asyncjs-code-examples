function* myGenerator(n) {
  const a = (yield 10) + n;
  yield a;
}

const g = myGenerator(1);
const v1 = g.next().value; // --> 10
const v2 = g.next(100).value; // --> 101

console.log(v1, v2);

/*
 * Read the content from a file
 * and write to another file using async await.
 * Includes error handling.
 */
const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

async function readWrite() {
  try {
    const content = await readFile('./example.txt', 'utf-8');
    const result = await writeFile('./example-copy.txt', content);
    return result;
  } catch (error) {
    console.log('An error happened while copying the file.');
    return Promise.reject(error);
  }
}

readWrite()
  .then(d => console.log(d, 'success!'))
  .catch(e => console.log(e))
  .then(d => console.log('Done with all operations.'));

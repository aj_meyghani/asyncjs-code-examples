/*
 * The example below demonstrates how to wrap a callback-based
 * function with a promise.
 */
const fs = require('fs');
function readFile(file, format) {
  format = format || 'utf-8';
  function handler(resolve, reject) {
    fs.readFile(file, format, function(err, content) {
      if(err) {
        return reject(err);
      }
      return resolve(content);
    });
  }
  const promise = new Promise(handler);
  return promise;
}


readFile('./example.txt')
  .then(content => console.log(content))
  .catch(err => console.log(err));

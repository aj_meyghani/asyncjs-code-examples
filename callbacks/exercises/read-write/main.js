const fs = require('fs');
const request = require('request');
const url = 'https://jsonplaceholder.typicode.com/posts/2';

request.get(url, handleResponse);
function handleResponse(err, resp, body) {
  if(err) throw new Error;
  const post = JSON.parse(body);
  const title = post.title;
  fs.readFile('./file.txt', 'utf-8', readFile(title));
}
const readFile = title => (err, content) => {
  if(err) throw new Error(err);
  const result = title + '\n' + content;
  fs.writeFile('./result.txt', result , writeResult);
}
function writeResult(err) {
  if(err) throw new Error(err);
  console.log('done');
}

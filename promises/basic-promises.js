/** ****** clear console *********/
console.clear();
/** ******************************/

/* MDN doc link:
  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise */

/* Note that both methods are static methods
that exist on the Promise global constructor
function/object.
*/

/* Create a simple promise */
let p1 = new Promise((resolve) => resolve(2));
p1
 .then((v) => console.log('value is', v))
 .catch((e) => console.log(e))
 .then((f) => console.log('Done with all'));

/* a promise that has reject as well */
let p2 = new Promise((resolve, reject) => {
    reject(new Error('some error happened'));
});

p2
 .then((d) => console.log(d))
 .catch((e) => console.log(e));

 /* if you just want to create a promise
 object with a set resolve value, you
 can use Promise.resolve
 */

let p3 = Promise.resolve(3);
p3
 .then((d) => console.log('value is', d))
 .catch((e) => console.log(e));


/* same is true for creating a promise object
that rejects a value
*/

let p4 = Promise.reject(new Error('Simple error'));
p4
 .then((d) => console.log(d))
 .catch((e) => console.log('error happened', e))
 .then('done p4');


/* A method that is very useful is the `all`
method on a promise instance. Using this you
can say you need to wait for all the promises
to resolve and you dont really care about the
order that much.
*/

let promises = [
    Promise.resolve('t1'),
    Promise.resolve('t2'),
    Promise.resolve('t3'),
];

/* calls then only after all the promises
have been resolved. The 'then' callback
will receive an array of results in the order
that the promises were passed in.
*/
Promise.all(promises)
 .then((results) => console.log(results));

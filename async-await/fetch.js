/* using the fetch api in the browser */
async function getPost() {
    var url = 'https://jsonplaceholder.typicode.com/posts/1';
    var response = await fetch(url);
    var post = await response.json();
    return post;
}

getPost()
    .then(post => console.log(post.id, post.title, post.body))
    .catch(e => console.log(e))
    .then(_ => console.log('Done'));

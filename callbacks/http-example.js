/**
 * Make a GET request to a remote api server.
 */
const https = require('https');
const url = 'https://jsonplaceholder.typicode.com/posts/1';

https.get(url, function(response) {
  response.setEncoding('utf-8');
  let body = '';
  response.on('data', (d) => {
    body += d;
  });
  response.on('end', (x) => {
    console.log(body);
  });
});

/*
* Read from one file and make a copy
* by writing the content to a new file
*/
const fs = require('fs');
fs.readFile('./notes.txt', 'utf-8', function(readErr, content) {
  if (readErr) {
    return console.log(readErr);
  }
  fs.writeFile('./notes-copy.txt', content, function(writeErr) {
    if (writeErr) {
      return console.log(writeErr);
    }
    console.log('Done copy');
  });
});


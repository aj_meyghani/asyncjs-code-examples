/*
 * The example below demonstrate some of the
 * static methods defined on the global Promise object.
 */
function getData() {
  return Promise.resolve('some data');
}
getData()
.then(d => console.log(d));


function rejectPromise() {
  return Promise.reject(new Error('something went wrong'));
}
rejectPromise()
.catch(e => console.log(e));


const p1 = Promise.resolve('v1');
const p2 = Promise.resolve('v2');
const p3 = Promise.resolve('v3');

const all = Promise.all([p1, p2, p3]);

all.then(values => console.log(values[0], values[1], values[2]));

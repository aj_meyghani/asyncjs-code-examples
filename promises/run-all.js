function taskA() {
  return Promise.resolve('task A');
}
function taskB() {
  return Promise.resolve('task B');
}
function taskC() {
  return Promise.resolve('task C');
}

function runAll() {
  const p1 = taskA();
  const p2 = taskB();
  const p3 = taskC();
  return Promise.all([p1, p2, p3]);
}

runAll()
  .then(d => console.log(d, 'all done'))
  .catch(e => console.log(e));

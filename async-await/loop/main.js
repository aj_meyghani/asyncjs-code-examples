const fs = require('fs');
const axios = require('axios');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

const files = ['./a.txt', './b.txt', './c.txt'];

async function operation(f) {
  const r1 = await readFile(f, 'utf-8');
  const r2 = await writeFile(f + '-copy.txt', r1);
  return r2;
}

const tasksPromises = files.map(operation);

Promise.all(tasksPromises)
  .then(r => console.log(r))
  .catch(e => console.log(e))
  .then(_ => console.log('all done'));

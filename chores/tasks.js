function laundry() {
  console.log('Start Laundry');
  return new Promise((resolve) => {
    setTimeout(function() {
      console.log('Finished Laundry');
      resolve('done laundry');
    }, 1000);
  });
}

function groceries() {
  console.log('Start groceries');
  return new Promise((resolve) => {
    setTimeout(function() {
      console.log('Finished groceries');
      resolve(['meat', 'oil', 'other ingredients']);
    }, 500);
  });
}

function cook(ingredients) {
  console.log('Start cooking');
  return new Promise((resolve) => {
    setTimeout(function() {
      console.log('Finished cooking');
      resolve('Done cooking with' + ingredients);
    }, 500);
  });
}

module.exports = {
  laundry: laundry,
  groceries: groceries,
  cook: cook,
};

/*
  Simple example to make a GET request to an API server.
  Axios is a promise-based http client that uses promises by default.
  The Axios module can be installed just like any other npm package:

  `npm install axios -S`

  As a reminder `-S` will add the package to the production entry
  in the `package.json` file
 */
const axios = require('axios');
function getDataFromServer() {
  const result =  axios.get('https://jsonplaceholder.typicode.com/posts/1');
  return result;
}


getDataFromServer()
  .then(function(response) {
    console.log(response);
  })
  .catch(function(error) {
    console.log(error);
  });

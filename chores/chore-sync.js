/*
 Example to point out how three tasks can be scheduled
 asynchronously. Also involes one task being dependent
 on the result of the previous task. Example shows
 that even with promises you can end up with a result
 that looks like a synchronous execution.
 */


const {laundry, groceries, cook} = require('./tasks');

/* my chores list (sync)

1) do laundry (takes 1 hour)
2) do groceries (takes 30 min)
3) do groceries (takes 30 min)

1 hour + 30 min + 30 min = 2hours
0-----------30min-----------1hr-----------1.5h-----------2h----------2.5h
|                           |      |                |       |          |
start                    finish   start          finish    start      finish
laundry                  laundry  groceries      groceries cooking    cooking
*/

function doTasksSync() {
  console.time('sync duration');
  return laundry()
    .then(function() {
      return groceries();
    })
    .then(function(ingredients) {
      return cook(ingredients);
    });
}

doTasksSync()
  .then(function() {
    console.log('done');
    console.timeEnd('sync duration');
  });

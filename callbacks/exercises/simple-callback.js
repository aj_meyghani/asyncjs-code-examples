function compute(nums, fn) {
  if(!Array.isArray(nums)) return NaN;
  const isAnyNotInteger = nums.some(v => !Number.isInteger(v));
  if(isAnyNotInteger) return NaN;
  return fn(nums);
}

const addAll = nums => nums.reduce((c, v) => c + v);
const result = compute([1,2,3], addAll);

console.log(result);

/*
 Example to point out how three tasks can be scheduled
 asynchronously. Also involes one task being dependent
 on the result of the previous task. Example shows
 that even with promises you can end up with a result
 that looks like a synchronous execution.
 */

const {laundry, groceries, cook} = require('./tasks');

async function doTasks() {
  console.time('duration');
  const laundryResult = laundry();
  const ingredients = await groceries();
  const cookingResult = await cook(ingredients);
  return Promise.all([laundryResult, cookingResult]);
}

doTasks().then((d) => {
  console.log('End result', d);
  console.timeEnd('duration');
});

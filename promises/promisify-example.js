/*
 * Example demonstrating how to use the `util.promisify`
 * method to create a promise-based version of a
 * callback function.
 */
const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);

readFile('./example.txt', 'utf-8')
  .then(content => console.log(content))
  .catch(err => console.log(err));

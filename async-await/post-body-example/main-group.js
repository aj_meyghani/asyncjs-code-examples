const fs = require('fs');
const axios = require('axios');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const readdir = util.promisify(fs.readdir);

function onlyTextFiles(f) {
  return f.split('.')[1] === 'txt';
}

async function readLocalContent() {
  const localFolderList = await readdir('.');
  const textFiles = localFolderList.filter(onlyTextFiles);
  const localFileContent = await readFile(textFiles[0], 'utf-8');
  return localFileContent;
}

async function getPostObject() {
  const response = await axios.get('https://jsonplaceholder.typicode.com/posts/1');
  return response.data.body;
}

async function main() {
  try {
    const results = await Promise.all([readLocalContent(), getPostObject()]);
    const finalContent = results[0] + results[1];
    return writeFile('./final2.txt', finalContent);
  } catch(e) {
    return Promise.reject(e);
  }
}

main()
  .then(d => console.log(d, 'success'))
  .catch(e => console.log('An error happened', e))
  .then(_ => console.log('All done'));


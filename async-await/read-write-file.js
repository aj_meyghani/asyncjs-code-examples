/*
 * Read the content from a file
 * and write to another file using async await.
 */
const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

async function readWrite() {
  const content = await readFile('./example.txt', 'utf-8');
  const result = await writeFile('./example-copy.txt', content);
  return result;
}

readWrite()
  .then(d => console.log(d, 'done'))
  .catch(e => console.log(e));

/*
 * In the example below we read the content of a file
 * replace the content, and write the result to another file.
 */
const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const writeToFile = util.promisify(fs.writeFile);


function removeInvalidChracters(str) {
  return Promise.resolve(str.replace(/-[xy]/g, ''));
}

/*
 * By returning the promise inside the `then` method
 * each promise has to be resolved before moving
 * onto the next task.
 */
const promiseChain = readFile('example.txt', 'utf-8')
  .then(function(content) {
    return removeInvalidChracters(content);
  })
  .then(function(cleanContent) {
    return writeToFile('./clean-file.txt', cleanContent);
  })
  .then(function() {
    console.log('done');
  })
  .catch(function(error) {
    console.log(error);
  });
